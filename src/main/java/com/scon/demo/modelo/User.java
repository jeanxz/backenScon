package com.scon.demo.modelo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="usuarios")
@Access(AccessType.FIELD)
public class User extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -96148364255192434L;
	
	@Column(name="nombre")
	private String nombre ;
	
	
	@Column(name="apellido",nullable =false ,length=255)
	private String apellido;
	
	@Column(name="tipo",nullable =false ,length=255)
	private String tipo;
	
	@Column(name="clave",nullable =false ,length=255)
	private String clave;
	
	@Column(name="userName",nullable =false ,length=255)
	private String userName;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
	
	
}
