package com.scon.demo.service;

import com.scon.demo.modelo.User;

public interface UserService {

	User save(User user);

}
