package com.scon.demo.util;

public class RestResponse {
	
	
	
	
	
	public RestResponse(Integer responceCode) {
		super();
		this.responceCode = responceCode;
	}
	
	
	public RestResponse(Integer responceCode, String message) {
		super();
		this.responceCode = responceCode;
		this.message = message;
	}


	private Integer responceCode;
	private String message;
	
	public Integer getResponceCode() {
		return responceCode;
	}
	public void setResponceCode(Integer responceCode) {
		this.responceCode = responceCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
